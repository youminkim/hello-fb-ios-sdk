//
//  main.m
//  hello_v4
//
//  Created by Youmin Kim on 3/10/15.
//  Copyright (c) 2015 y2b. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
