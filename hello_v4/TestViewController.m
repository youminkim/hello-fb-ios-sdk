//
//  TestViewController.m
//  hello_v4
//
//  Created by Youmin Kim on 4/20/15.
//  Copyright (c) 2015 y2b. All rights reserved.
//

#import "TestViewController.h"

@interface TestViewController () <UITableViewDataSource, UITableViewDelegate, FBSDKLoginButtonDelegate, FBSDKAppInviteDialogDelegate, UIImagePickerControllerDelegate>

@end

@implementation TestViewController
{
    UITableView *tv;
    NSArray *tableData;
    FBSDKLoginManager *loginManager;
    UIImage *localImage;
    
}

FBSDKShareLinkContent *linkContent;
FBSDKSharePhotoContent *linkPhotoContent;
FBSDKSharePhotoContent *localPhotoContent;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadTableData];
    
    tv = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-50)];
    [tv setSectionHeaderHeight:50.f];
    [tv setDelegate:self];
    [tv setDataSource:self];
    [self.view addSubview:tv];
    
    [self initData];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    if (!localPhotoContent) {
        [self initLocalPhoto];
    }

}

- (void) initData {
    
    // link
    NSString *linkURL = @"https://www.friendsmash.com";
    NSString *pictureURL = @"http://www.friendsmash.com/images/logo_large.jpg";
    linkContent = [[FBSDKShareLinkContent alloc] init];
    [linkContent setContentURL:[NSURL URLWithString:linkURL]];
    [linkContent setContentTitle:@"Checkout my Friend Smash greatness!"];
    [linkContent setContentDescription:@"Come smash me back!"];
    [linkContent setImageURL:[NSURL URLWithString:pictureURL]];
    
    // link photo
    FBSDKSharePhoto *photo = [FBSDKSharePhoto photoWithImageURL:[NSURL URLWithString:@"http://parse.com/images/og/default.jpg"] userGenerated:YES];
    linkPhotoContent = [[FBSDKSharePhotoContent alloc] init];
    linkPhotoContent.photos = @[photo];
}

- (void) initLocalPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    localImage = info[UIImagePickerControllerOriginalImage];
    
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = localImage;
    photo.userGenerated = YES;
    localPhotoContent = [[FBSDKSharePhotoContent alloc] init];
    localPhotoContent.photos = @[photo];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - App Events

- (void) clickedAppEventsPurchase {
    [FBSDKAppEvents logPurchase:4.32 currency:@"USD"];
}

#pragma mark - Share
- (void) clickedShareAPIButton: (id<FBSDKSharingContent>)content {
    [FBSDKShareAPI shareWithContent:content delegate:nil];
}

- (void) clickedShareSheetButton: (id<FBSDKSharingContent>)content {
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = self;
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeShareSheet;
    [dialog show];
}

- (void) clickedShareButton: (id<FBSDKSharingContent>)content {
    [FBSDKShareDialog showFromViewController:nil withContent:content delegate:self];
}

- (void) clickedMessageDialog: (id<FBSDKSharingContent>)content {
    
    [FBSDKMessageDialog showWithContent:content delegate:nil];
}

- (void) clickedAppInviteButton {
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
    content.appLinkURL = [NSURL URLWithString:@"http://youminkim-test.parseapp.com/applinks/beat.html"];
    //optionally set previewImageURL
    content.previewImageURL = [NSURL URLWithString:@"http://youminkim-test.parseapp.com/imgs/cat_1200x630.jpg"];
    
    // present the dialog. Assumes self implements protocol `FBSDKAppInviteDialogDelegate`
    [FBSDKAppInviteDialog showWithContent:content
                                 delegate:self];
}

/*!
 @abstract Sent to the delegate when the share completes without error or cancellation.
 @param sharer The FBSDKSharing that completed.
 @param results The results from the sharer.  This may be nil or empty.
 */
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    
}

/*!
 @abstract Sent to the delegate when the sharer encounters an error.
 @param sharer The FBSDKSharing that completed.
 @param error The error.
 */
- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    
}

/*!
 @abstract Sent to the delegate when the sharer is cancelled.
 @param sharer The FBSDKSharing that completed.
 */
- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    
}

#pragma mark - Request
- (void)clickedRequestButton {
    FBSDKGameRequestContent *content = [[FBSDKGameRequestContent alloc] init];
    [content setMessage:[NSString stringWithFormat:@"Come join me in the friend smash times!"]];
    [content setTitle:@"Smashing Invite!"];
    
    [FBSDKGameRequestDialog showWithContent:content delegate:self];
}

- (void)gameRequestDialog:(FBSDKGameRequestDialog *)appRequestDialog didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"%@", results);
}

- (void)gameRequestDialog:(FBSDKGameRequestDialog *)appRequestDialog didFailWithError:(NSError *)error {
    NSLog(@"%@", error);
}

- (void)gameRequestDialogDidCancel:(FBSDKGameRequestDialog *)appRequestDialog {
    NSLog(@"%@", @"canceled");
}

#pragma mark - Login
- (void)clickedLoginButton {
    
    if (FBSDKAccessToken.currentAccessToken) {
        NSLog(@"Already logined");
        [self userLoggedIn:FBSDKAccessToken.currentAccessToken];
    } else {
        NSLog(@"Login start");
        [self login];
    }
}

- (void) login {
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"email", @"public_profile", @"user_friends",
                            nil];
    if (!loginManager) {
        loginManager = [[FBSDKLoginManager alloc] init];
//        loginManager.loginBehavior = FBSDKLoginBehaviorWeb;
    }
    
    [loginManager logInWithReadPermissions:permissions
                                   handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                       NSLog(@"After Login Dialog ");
                                       if ([FBSDKAccessToken currentAccessToken]) {
                                           NSLog(@"Token");
                                       } else if (result.isCancelled) {
                                           NSLog(@"Canceled");
                                       } else if (error) {
                                           NSLog(@"Error");
                                       }
                                   }];
    

}

- (void) login_publish_actions {
    [loginManager logInWithPublishPermissions:[NSArray arrayWithObjects:@"publish_actions", nil] handler:nil];
}

- (void)userLoggedIn: (FBSDKAccessToken *)token {
    NSLog(@"Logged In");
        [self login_publish_actions];
}

- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error {
    NSLog(@"Login didCompleteWithResult");
    
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
     NSLog(@"Login loginButtonDidLogOut");
}

#pragma mark - App Invite
- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"App Invite - didCompleteWithResults");
    NSLog(@"%@", results);
}
- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error {
    NSLog(@"App Invite - didFailWithError");
    NSLog(@"%@", error);
}

#pragma mark - Table

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [self clickedLoginButton];
            break;
        case 1:
            [self clickedRequestButton];
            break;
        case 2:
            [self clickedAppInviteButton];
            break;
        case 3:
            [self clickedAppEventsPurchase];
            break;
        case 4:
            [self clickedShareButton:linkContent];
            break;
        case 5:
            [self clickedShareButton:linkPhotoContent];
            break;
        case 6:
            [self clickedShareButton:localPhotoContent];
            break;
        case 7:
            [self clickedShareAPIButton:linkContent];
            break;
        case 8:
            [self clickedShareAPIButton:linkPhotoContent];
            break;
        case 9:
            [self clickedShareAPIButton:localPhotoContent];
            break;
        case 10:
            [self clickedShareSheetButton:linkContent];
            break;
        case 11:
            [self clickedShareSheetButton:linkPhotoContent];
            break;
        case 12:
            [self clickedShareSheetButton:localPhotoContent];
            break;
        case 13:
            [self clickedMessageDialog:linkContent];
            break;
        case 14:
            [self clickedMessageDialog:linkPhotoContent];
            break;
        case 15:
            [self clickedMessageDialog:localPhotoContent];
            break;
        default:
            break;
    }
}

- (void) loadTableData {
    tableData = [NSArray arrayWithObjects:
                 @"Login",
                 @"Game request",
                 @"App Invite",
                 @"App Events - Purchase",
                 @"Share Dialog - Link",
                 @"Share Dialog - Photo (link)",
                 @"Share Dialog - Photo (local)",
                 @"Share API - Link",
                 @"Share API - Photo (link)",
                 @"Share API - Photo (local)",
                 @"Share Sheet - Link",
                 @"Share Sheet - Photo (link)",
                 @"Share Sheet - Photo (local)",
                 @"Message Dialog - Link",
                 @"Message Dialog - Photo (link)",
                 @"Message Dialog - Photo (local)",

                 nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 100)];

    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width/2, 40)];
    [view addSubview:loginButton];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"tableItems";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
