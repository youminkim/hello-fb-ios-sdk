//
//  AppDelegate.h
//  hello_v4
//
//  Created by Youmin Kim on 3/10/15.
//  Copyright (c) 2015 y2b. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

